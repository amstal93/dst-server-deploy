return {
  ["workshop-1320336258"]={ configuration_options={  }, enabled=true },
  ["workshop-358749986"]={
    configuration_options={ IndicatorSize=3, MaxIndicator=7000, PlayerIndicators=1 },
    enabled=true 
  },
  ["workshop-362175979"]={ configuration_options={ ["Draw over FoW"]="disabled" }, enabled=true },
  ["workshop-375859599"]={
    configuration_options={
      divider=4,
      random_health_value=0,
      random_range=0,
      show_type=2,
      unknwon_prefabs=1,
      use_blacklist=true 
    },
    enabled=true 
  },
  ["workshop-378160973"]={
    configuration_options={
      ENABLEPINGS=true,
      FIREOPTIONS=2,
      OVERRIDEMODE=false,
      SHAREMINIMAPPROGRESS=true,
      SHOWFIREICONS=true,
      SHOWPLAYERICONS=true,
      SHOWPLAYERSOPTIONS=2 
    },
    enabled=true 
  },
  ["workshop-458940297"]={
    configuration_options={
      DFV_ClientPrediction="default",
      DFV_FueledSettings="default",
      DFV_Language="EN",
      DFV_MinimalMode="default",
      DFV_PercentReplace="default",
      DFV_ShowACondition="default",
      DFV_ShowADefence="default",
      DFV_ShowAType="default",
      DFV_ShowDamage="default",
      DFV_ShowFireTime="default",
      DFV_ShowInsulation="default",
      DFV_ShowTemperature="default",
      DFV_ShowUses="default" 
    },
    enabled=true 
  },
  ["workshop-569043634"]={ configuration_options={  }, enabled=true },
  ["workshop-758532836"]={
    configuration_options={
      AUTOPAUSECONSOLE=false,
      AUTOPAUSEMAP=false,
      AUTOPAUSESINGLEPLAYERONLY=true,
      ENABLECLIENTPAUSE=true,
      ENABLEHOTKEY=false,
      KEYBOARDTOGGLEKEY="P" 
    },
    enabled=true 
  },
  ["workshop-767776640"]={ configuration_options={ bonus=0.1, maxweapon=1 }, enabled=true },
  ["workshop-862195447"]={
    configuration_options={ Ownership=false, craft=0, maxchesters=1, science=1 },
    enabled=true 
  } 
}
