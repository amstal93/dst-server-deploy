# Change Log

## [next]

## [1.2.1]
* Fixed bug resetting player progress when server updated game or mods.
* Update repo documentation.
* Switched to using auto restarding containers by default
* Switched to using a default dst-net network. 

## [1.1.0]
* Added restrictions on python and PyYAML version in setup.py
* Centralized license information and added Klei license information for the presets.
* Added reference csv files which can be ingested by the script.
* Added overwrite warnings.
* Added arg parser for cli arguments.

## [1.0.0]
* Initial release.

[next]: https://gitlab.com/lego_engineer/dst-server-deploy/compare/1.2.1...HEAD
[1.1.1]: https://gitlab.com/lego_engineer/dst-server-deploy/compare/1.1.0...1.2.1
[1.1.0]: https://gitlab.com/lego_engineer/dst-server-deploy/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.com/lego_engineer/dst-server-deploy/compare/307c92971218c1a1e4c77d61affac2037f9b0ba9...1.0.0
